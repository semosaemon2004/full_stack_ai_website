import { NextApiResponse, NextApiRequest } from 'next';

// Custom middleware function to bypass authentication for all routes
export default async function middleware(req: NextApiRequest, res: NextApiResponse) {
  // Bypass authentication for all requests
  return;
}

// Exporting config to specify API middleware behavior
export const config = {
  api: {
    bodyParser: false,
  },
};
