import Image from "next/image";
import { Collection } from "@/components/shared/Collection";
import Header from "@/components/shared/Header";
import { getUserImages } from "@/lib/actions/image.actions";
import { getUserById } from "@/lib/actions/user.actions";

const Profile = async ({ searchParams }: SearchParamProps) => {
  const page = Number(searchParams?.page) || 1;

  // Remove the authentication check and redirection logic

  let user;
  try {
    user = await getUserById("defaultUserId"); // Provide a default user ID
  } catch (error) {
    console.error("Error fetching user:", error);
    // Handle the error here
  }

  // Set a default value for creditBalance only if user is undefined
  const creditBalance = user ? user.creditBalance : 10000;

  let images;
  try {
    images = await getUserImages({ page, userId: user?._id });
  } catch (error) {
    console.error("Error fetching user images:", error);
    // Handle the error here
    images = { data: [], totalPages: 1 };
  }

  return (
    <>
      <Header title="Profile" />

      <section className="profile">
        <div className="profile-balance">
          <p className="p-14-medium md:p-16-medium">CREDITS AVAILABLE</p>
          <div className="mt-4 flex items-center gap-4">
            <Image
              src="/assets/icons/coins.svg"
              alt="coins"
              width={50}
              height={50}
              className="size-9 md:size-12"
            />
            {/* Use the creditBalance variable */}
            <h2 className="h2-bold text-dark-600">{creditBalance}</h2>
          </div>
        </div>

        <div className="profile-image-manipulation">
          <p className="p-14-medium md:p-16-medium">IMAGE MANIPULATION DONE</p>
          <div className="mt-4 flex items-center gap-4">
            <Image
              src="/assets/icons/photo.svg"
              alt="coins"
              width={50}
              height={50}
              className="size-9 md:size-12"
            />
            <h2 className="h2-bold text-dark-600">{images?.data.length}</h2>
          </div>
        </div>
      </section>

      <section className="mt-8 md:mt-14">
        <Collection
          images={images?.data}
          totalPages={images?.totalPages}
          page={page}
        />
      </section>
    </>
  );
};

export default Profile;
