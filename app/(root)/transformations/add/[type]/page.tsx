import Header from '@/components/shared/Header'
import TransformationForm from '@/components/shared/TransformationForm';
import { transformationTypes } from '@/constants'

const AddTransformationTypePage = ({ params: { type } }: SearchParamProps) => {
  // Fetch transformation based on type
  const transformation = transformationTypes[type];

  // Provide default values for userId and creditBalance
  const userId = "defaultUserId";
  const creditBalance = 10000; // Provide a default value or handle it according to your requirements

  return (
    <>
      <Header 
        title={transformation.title}
        subtitle={transformation.subTitle}
      />
    
      <section className="mt-10">
        <TransformationForm 
          action="Add"
          userId={userId}
          type={transformation.type as TransformationTypeKey}
          creditBalance={creditBalance}
        />
      </section>
    </>
  )
}

export default AddTransformationTypePage
